class CreateCylinders < ActiveRecord::Migration[5.0]
  def change
    create_table :cylinders do |t|
      t.string :pin
      t.string :valve
      t.date :production
      t.string :gas
      t.float :pressure
      t.string :project
      t.string :vender
      t.string :manufacturer
      t.string :agency
      t.string :user
      t.string :mac
      t.string :location

      t.timestamps
    end
  end
end
