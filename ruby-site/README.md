# App Information

This is some documentation for different parts (static and dynamic) of the website.

## Changing the Site  

* CSS/Js can be changed in `app/assets`  
* The html.erb (views) that make up the site can be found in `app/views`  
* The responses to different HTTP requests (e.g. GET, POST) are defined in controller logic for each Model in `app/controllers`  
* The site layout and information can be changed in:  
  * `app/views/layouts/application.html.erb`  
  * `app/views/layouts/_navigaton.html.erb`  
  * `app/views/layouts/_navigation_links.html.erb`  
  * `app/views/layouts/_nav_links_for_auth.html.erb`  
* The form for cylinder creation can be changed in `app/views/cylinder/_form.html.erb`  

## Adding to the Site  
* Use `rails g Scaffold object` to create the Model, View, and Controller for `object`  
* Run `rake db:migrate` after creating the new thing  
