class AddUserReferencesToCylinders < ActiveRecord::Migration[5.0]
  def change
    add_reference :cylinders, :user, foreign_key: true
  end
end
