class Cylinder < ApplicationRecord
	has_many :user_cylinders
	has_many :users, :through => :user_cylinders

	# try 2
	#include Filterable

	# try 1
	# here or in controller?
	def self.search(search)
	  search_condition = "%" + search + "%"
	  #byebug
	  Cylinder.where(:conditions => ['pin LIKE ? OR valve LIKE ?', search_condition, search_condition])
	end

end
