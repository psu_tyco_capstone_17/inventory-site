class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def ensure_authentication
  	# check this (do you need all roles?)
  	unless user_signed_in? #or admin_signed_in?
  		redirect_to login_path, alert: "You have to be signed in" and return
  	end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

end
