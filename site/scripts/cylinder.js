

// Function takes the cylinder PIN and queries the server for it
function GetCylinder(id) {
	//alert(id);

	var pin = "34";
	var baseurl = "http://localhost:5000/customer/";

	var settings = {
	  "async": true,
	  "crossDomain": true,
	  "url": baseurl+pin,
	  "method": "GET",
	  "headers": {

	  	/* Temporary headers (for development)*/
		"Access-Control-Allow-Origin": "*",
		"Access-Control-Allow-Methods": "GET",
		"Access-Control-Allow-Headers" : "Authorization, Lang",
		//*/

		// Need to include auth headers

		"cache-control": "no-cache",
		"postman-token": "23ea0cbc-37ea-2790-3ad6-dd55d5013c79"
	  }
	};

	$.ajax(settings).done(function (data) {
		var items = [];
		$.each( data, function( key, val ) {
			items.push("<td>" + key + "</td>");
			items.push("<td>" + val + "</td>");

			$( "<tr/>", {
				"class": "key",
				html: items.join( "" )
			}).appendTo( "tbody" );

			items = [];
		});

	});
}
