FactoryGirl.define do
  factory :cylinder do
    pin "MyString"
    valve "MyString"
    production "2017-04-25"
    gas "MyString"
    pressure 1.5
    project "MyString"
    vender "MyString"
    manufacturer "MyString"
    agency "MyString"
    user "MyString"
    mac "MyString"
    location "MyString"
  end
end
