class DropUserFromCylinders < ActiveRecord::Migration[5.0]
  def change
  	remove_column :cylinders, :user_id
  end
end
