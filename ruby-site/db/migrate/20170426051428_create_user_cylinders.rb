class CreateUserCylinders < ActiveRecord::Migration[5.0]
  def change
    create_table :user_cylinders do |t|
      t.references :user, foreign_key: true
      t.references :cylinder, foreign_key: true

      t.timestamps
    end
  end
end
