# Inventory Management System
## Tyco International

This website uses Ruby on Rails (currently hosted on heroku) with a PostgreSQL database.

To run this on a local server, you must install (and set up proper permissions for):

* `Ruby`
* `Rails`
* `PostgreSQL` (this service needs to be run while the server is running)

If app setup has to be redone for any reason, you can follow these commands for setup on a paid dyno at Heroku or a company-owned server.

* create a Rails app with `rails new appname`  
* push to Heroku (optional)  
    * `$ heroku create TycoSite`  
    * `$ heroku git:remote -a TycoSite`  
    * push the `ruby-tree/` directory to Heroku with `git subtree push --prefix ruby-site heroku master`  
* After setting up server and installing Ruby and Rails, install and migrate necessary information:  
    * `$ bundle install`  
    * `bin/rake db:create db:migrate`  
    * `bin/rails db:migrate RAILS_ENV=development`  
* If this was pushed to Heroku, use:
    * `$ heroku run rake db:create db:migrate`  

### Administration  

* To use the console, run `rails c` to enter the Rails console  
* To log into Heroku console, run `heroku run bash`  
    * Note - this project may require the use of non-free dynos on Heroku

See more information in `ruby-site/README.md`.

For best results with a site, it might help to have a contractor manage your website, rather than getting temporary workers to write it (students cannot provide any more maintenance for the site).

Author: Amar Paul, 2017
