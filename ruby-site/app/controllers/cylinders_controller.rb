class CylindersController < ApplicationController
  # ensure auth before anyone sees
  before_action :ensure_authentication
  before_action :product_owner_only, only: [:show, :edit, :update, :destroy]

  # before you can check who owns cylinder, set cylinder
  before_action :set_cylinder, only: [:show, :edit, :update, :destroy]

  # search for cylinders (pin or valve pin ?)
  def search
    search_condition = "%" + search + "%"
    byebug
    @cylinder = Cylinder.where(:conditions => ['pin LIKE ? OR valve LIKE ?', search_condition, search_condition])
    byebug
    #@cylinder = Cylinder.search params[:search]
  end

  # GET /cylinders
  # GET /cylinders.json
  def index
    if current_user.admin?
      @cylinders = Cylinder.all
    else
      @cylinders = current_user.cylinders
    end
  end

  # GET /cylinders/1
  # GET /cylinders/1.json
  def show
  end

  # GET /cylinders/new
  def new
    @cylinder = Cylinder.new
  end

  # GET /cylinders/1/edit
  def edit
  end

  # POST /cylinders
  # POST /cylinders.json
  def create
    @cylinder = Cylinder.new(cylinder_params)
    @cylinder.user = current_user

    # create the user-to-cylinder mapping
    uc = UserCylinder.new
    uc.cylinder = @cylinder
    uc.user = current_user
    uc.save!

    respond_to do |format|
      if @cylinder.save
        format.html { redirect_to @cylinder, notice: 'Cylinder was successfully created.' }
        format.json { render :show, status: :created, location: @cylinder }
      else
        format.html { render :new }
        format.json { render json: @cylinder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cylinders/1
  # PATCH/PUT /cylinders/1.json
  def update
    respond_to do |format|
      if @cylinder.update(cylinder_params)
        format.html { redirect_to @cylinder, notice: 'Cylinder was successfully updated.' }
        format.json { render :show, status: :ok, location: @cylinder }
      else
        format.html { render :edit }
        format.json { render json: @cylinder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cylinders/1
  # DELETE /cylinders/1.json
  def destroy
    @cylinder.destroy
    respond_to do |format|
      format.html { redirect_to cylinders_url, notice: 'Cylinder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cylinder
      @cylinder = Cylinder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cylinder_params
      params.require(:cylinder).permit(:pin, :valve, :production, :gas, :pressure, :project, :vender, :manufacturer, :agency, :user, :mac, :location)
    end

    def product_owner_only
      @cylinder = Cylinder.find(params[:id])
      unless @cylinder.users.include?(current_user) or current_user.admin?
      #unless (signed_in? and current_user == @cylinder.user) or current_user.admin?
        redirect_to cylinders_url, alert: "You don't have permission to access this cylinder" and return
      end
    end

end
