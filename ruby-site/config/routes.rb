Rails.application.routes.draw do
  resources :cylinders
  root to: 'visitors#index'
  devise_for :users
  resources :users

  post '/search' => "cylinders#search", :as => :search

  devise_scope :user do
    get '/users/sign-in' => "devise/sessions#new", :as => :login
  end

  devise_scope :user do
  	get '/users/edit' => "devise/sessions#edit", :as => :edit
  end
end
